const config = require('./config');
const { Client } = require('pg');
require('dotenv').config({path:'.env'});
keccak256 = require('js-sha3').keccak256;
const axios = require('axios');
const ratelimit = require('axios-rate-limit');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter(config.csv);
const client = new Client(config.db);
const http = ratelimit(axios.create(config.api), config.rate_limit);

GetAddresses()
.then(async rows => {
    for (let i=0; i < rows.length; i++) {
        process.stdout.write("Getting data from address: " + rows[i].address + "...");
        if(isValidAddress(rows[i].address)) {
            balance = await getCurrentAccountBalance(rows[i].address);
            tokens = await getAddressTokens(rows[i].address);
            metadata = await getAddressMetadata(rows[i].address);

            console.log('Done');
            csvWriter.writeRecords([{
                address: rows[i].address,
                balance_usd: balance.usd,
                balance_eth: balance.eth,
                assets: tokens,
                firstSeen: metadata.firstSeen,
                detailed_page: metadata.detailed_page
            }]);
        } else {
            console.log(config.invalid_address_msg);
        }
    }
})
.catch(error => {
    throw Error (error)
});

/**
 * Get all the wallet address in postgres database
 * 
 * @method getRows
 * @param {String} address
 * @return {Int}
 */
async function GetAddresses() {
    try {
        await client.connect();
        const res = await client.query(config.query);
        await client.end();
        return res.rows;
    }
    catch (err) {
        return err;
    }
}

/**
 * Get current balance given a wallet address
 * 
 * @method getCurrentAccountBalance
 * @param {String} address
 * @return {Int}
 */
async function getCurrentAccountBalance(address) {
    try {
        const error_msg = "Address Not found";
        let balance = {
            usd: error_msg,
            eth: error_msg
        };
        const params = "?includePrice=true&currency=usd";
        const response = await http.get('/' + address + '/account-balances/latest' + params)
        if(response.data.status === 404) {
            return balance;
        }

        usd = response.data.payload.price.value.total;
        if (usd) {
            balance.usd = parseFloat(usd).toFixed(2);
        }

        eth = response.data.payload.value;
        if (eth) {
            balance.eth = parseFloat(eth + "e-18").toFixed(2);
        }

        return balance;
    }
    catch (err) {
        return err.response.data.message;
    }
}

/**
 * Get all tokens given a wallet address
 * 
 * @method getAddressTokens
 * @param {String} address
 * @return {Array}
 */
async function getAddressTokens(address) {
    try {
        const response = await http.get('/' + address + '/tokens')
        if(response.data.status === 404) {
            return "No assets";
        }

        let assets;
        records = response.data.payload.records;
        if (records && records.length > 0) {
            assets = [];
            for (let i=0; i < records.length; i++) {
                assets.push(records[i].symbol);
            }
        } else {
            assets = "No assets";
        }
        return assets;
    }
    catch (err) {
        return err.response.data.message;
    }
}

/**
 * Get the ISO Formatted date of when the address made its first public appearance and the detailed page address.
 *
 * @method getAddressMetadata
 * @param {String} address
 * @return {String}
 */
async function getAddressMetadata(address) {
    try {
        const response = await http.get('/' + address + '/metadata')
        data = {
            firstSeen: "Not found",
            detailed_page: "Not found"
        };
        if (response.data.payload && response.data.payload.length > 0) {
            data.firstSeen = response.data.payload[0].firstSeen;
            data.detailed_page = response.data.payload[0].network.link;
        }
        return data;
    }
    catch (err) {
        return err.response.data.message;
    }
}

/**
 * Checks if the given string is an address
 *
 * @method isValidAddress
 * @param {String} address the given HEX adress
 * @return {Boolean}
*/
function isValidAddress(address) {
    if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
        return false;
    } else if (/^(0x)?[0-9a-f]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address)) {
        return true;
    } else {
        return isChecksumAddress(address);
    }
};

/**
 * Checks if the given string is a checksummed address
 *
 * @method isChecksumAddress
 * @param {String} address the given HEX adress
 * @return {Boolean}
*/
function isChecksumAddress(address) {
    address = address.replace('0x','');
    let addressHash = keccak256(address.toLowerCase());
    for (let i = 0; i < 40; i++ ) {
        if ((parseInt(addressHash[i], 16) > 7 && address[i].toUpperCase() !== address[i]) || (parseInt(addressHash[i], 16) <= 7 && address[i].toLowerCase() !== address[i])) return false;
    }
    return true;
};

function exitHandler(options, exitCode) {
    if (exitCode === 0) {
        console.log('File ' + config.csv.path + ' generated succesfully :)');
    }
    if (options.exit) process.exit();
}

process.on('exit', exitHandler.bind(null,{cleanup:true}));
