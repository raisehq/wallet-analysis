require('dotenv').config({path:'.env'});
const config = {
	db: {
		user: process.env.PG_USER,
    	host: process.env.PG_HOST,
    	database: process.env.PG_DATABASE,
    	password: process.env.PG_PASSWORD,
    	port: process.env.PG_PORT,
	},
	query: "SELECT a.address FROM crypto_address a INNER JOIN hero_user u ON a.herouser_id = u.id WHERE u.accounttype_id = 2",
	csv: {
		path: "info.csv",
		header: [
			{id: 'address', title: 'Address'},
			{id: 'balance_usd', title: 'Balance in USD'},
			{id: 'balance_eth', title: 'Balance in ETH'},
			{id: 'assets', title: 'Assets'},
			{id: 'firstSeen', title: 'First seen'},
			{id: 'detailed_page', title: 'Detailed page'}
		],
		fieldDelimiter: ";"
	},
	api: {
		headers: {
			'x-amberdata-blockchain-id': 'ethereum-mainnet',
			'x-api-key': process.env.AMBERDATA_API_KEY
		},
		params: {
			page: '0',
			size: '100'
		},
		baseURL: "https://web3api.io/api/v2/addresses/",
	},
	rate_limit: {
		maxRequests: 2,
		perMilliseconds: 2000,
		maxRPS: 2
	},
	invalid_address_msg: "Invalid wallet address"
};

module.exports = config;
