# Wallet analysis

This project tries to analize information about the our users wallets.
Save all the results into a csv file on the project folder called info.csv

## Prerequisites

- [Amberdata api key](https://amberdata.io/)
- [Nodejs](https://nodejs.org/)

## Dependencies

For install the dependencies just do: npm install

```bash
franky@franky-xps-13-9380  ~/token-analysis  npm install
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN token-data@1.0.0 No repository field.

added 23 packages from 17 contributors and audited 23 packages in 0.943s
found 0 vulnerabilities
```

- [CSV Writer](https://www.npmjs.com/package/csv-writer)
- [Dotenv](https://www.npmjs.com/package/dotenv)
- [Node Postgres](https://node-postgres.com/)
- [Axios](https://www.npmjs.com/package/axios)
- [Axios rate limit](https://www.npmjs.com/package/axios-rate-limit)

## Environment variables

| Name | Description | Default |
|------|-------------|:-----:|
| PG\_USER | Postgres database user with read permissions | n/a |
| PG\_PASSWORD | Postgres password for the user. | n/a |
| PG\_HOST | Postgres host database | n/a |
| PG\_PORT | Port where postgres database accept connections | 5432 |
| PG\_DATABASE | Postgres database name | n/a |
| AMBERDATA\_API\_KEY | Amberdata.io api key for send requests to the blockchain | n/a |

Fill the values of environment variables in file .env

```bash
franky@franky-xps-13-9380  ~/token-analysis  vim .env
PG_USER=db_user
PG_PASSWORD=db_password
PG_HOST=db_host
PG_PORT=5432
PG_DATABASE=db_database
AMBERDATA_API_KEY=1234
```

## Config.js

This file is used to config params about amberdata api, csv structure , rate-limit etc.

## Usage

For execute this project you just have to: node main.js

```bash
franky@franky-xps-13-9380  ~/wallet-analysis  node main.js
Getting data from address: 0xd30CCd055547B342BA1Fb9668927d8b9fe05551a...Done
Getting data from address: 0xd30CCd055547B342BA1Fb9668927d8b9fe05551a...Done
Getting data from address: 0xd30CCd055547B342BA1Fb9668927d8b9fe05551a...Done
Getting data from address: 0x57Ed5dB408B3Cf1b154d3600B0Ca3166Be1ABf0D...Done
....
File info.csv generated succesfully :)
```

## Methods

- [GetAddressTokens](https://docs.amberdata.io/reference#get-address-tokens): Get all the wallet address in postgres database.
- [GetCurrentAccountBalance](https://docs.amberdata.io/reference#get-current-account-balance): Get current balance given an wallet address.
- [GetAddressMetadata](https://docs.amberdata.io/reference#get-address-metadata): Get current balance given an wallet address.

## CSV

The headers of the csv are the following:

| Name | Description
|------|-------------|
| Address | User crypto adress
| Balance | Wallet balance in ethereum
| Assets | All the symbol assets separated by commas
| First seen | First time in date format that the wallet was created
| Detailed page | Link to address details page

This headers can be edited in config.js file.
The separator of the csv file is the symbol: ";".
You can change it in the config.js file.

## Authors

Module is maintained by [Francesc Armengol](https://gitlab.com/francesc-raise.it).
