#!/bin/bash

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

if [ -n "${1-}" ]; then
    ACTION=${1}
	if [[ ${ACTION} != "add" && ${ACTION} != "revoke" ]]; then
		echo "Invalid param action. Value: add, revoke."; exit 1;
	fi
else
	echo "Missing param action: ./add_revoke_permissions.sh add|revoke"; exit 1;
fi

echo "---------------------------------------------------------------------------"
echo "Getting public ip..."
echo "---------------------------------------------------------------------------"
MY_IP=$(curl ipecho.net/plain ; echo)

echo "---------------------------------------------------------------------------"
echo "Getting security group for postgres replica..."
echo "---------------------------------------------------------------------------"
SG_ID=$(aws rds describe-db-instances --query 'DBInstances[?DBInstanceIdentifier==`production-replica-postgres`]' --output text | grep VPCSECURITYGROUPS | awk '{print $3}')

if ! [[ ${MY_IP} =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
	echo "Can't detect your public ip." exit 1;
fi

if [[ ${ACTION} == "add" ]]; then
echo "---------------------------------------------------------------------------"
echo "Add ip ${MY_IP} permissions..."
echo "---------------------------------------------------------------------------"
aws ec2 authorize-security-group-ingress \
    --group-id  ${SG_ID} \
	--protocol tcp \
	--port 5432 \
	--cidr "${MY_IP}/32"
fi

if [[ ${ACTION} == "revoke" ]]; then
echo "---------------------------------------------------------------------------"
echo "Revoke ${MY_IP} permissions..."
echo "---------------------------------------------------------------------------"
aws ec2 revoke-security-group-ingress \
	--group-id ${SG_ID} \
	--protocol tcp \
	--port 5432 \
	--cidr "${MY_IP}/32"
fi
